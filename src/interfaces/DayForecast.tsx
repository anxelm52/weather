export interface DayForecastProps {
    temperature: Number;
    date: Date;
    type: string;
}