import { WeatherFilter } from './components/WeatherFilter'
import { WeatherHero } from './components/WeatherHero'
import { WeatherForecast } from './components/WeatherForecast'
import { consts } from './helpers/consts';

import forecastData from './data/forecast.json';

export const App = () => {

    const weekForecast = forecastData?.map((dayForecast, i) => {
        return { date: new Date(dayForecast.day), ...dayForecast }
    })?.slice(0 , consts.maxDaysForecast) ?? [];

    return (
        <main>
            <WeatherFilter />
            <WeatherHero />
            <WeatherForecast forecast={weekForecast} />
        </main>
    );
};
