import { DayForecastProps } from '../interfaces/DayForecast';
import { getDay } from '../helpers/datetimeHelpers';

export const DayForecast: React.FC<DayForecastProps> = props => {
    return (
        <div className={`day ${props.type}`}>
            <p>{props.date ? getDay(props.date) : 'N/A'}</p>
            <span>{props.temperature}</span>
        </div>
    );
};


