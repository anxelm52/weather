export const WeatherHero = () => {
    return (
        <>
            <div className="head">
                <div className="icon cloudy"></div>
                <div className="current-date">
                    <p>Пятница</p>
                    <span>29 ноября</span>
                </div>
            </div>
            <div className="current-weather">
                <p className="temperature">17</p>
                <p className="meta">
                    <span className="rainy">%35</span>
                    <span className="humidity">%72</span>
                </p>
            </div>
        </>);
};