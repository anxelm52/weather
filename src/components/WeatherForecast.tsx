import { DayForecastProps } from '../interfaces/DayForecast';
import { DayForecast } from './DayForecast';

export const WeatherForecast: React.FC<WeatherForecastProps> = (props) => {
    return (
        <div className="forecast">
            {props.forecast.map((dayForecast, i) => {
                return (<DayForecast {...dayForecast} />)
            })}
        </div>
    );
};

interface WeatherForecastProps{
    forecast: DayForecastProps[];
}
