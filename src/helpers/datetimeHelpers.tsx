export const getDay = (date: Date) => {
    return date.toLocaleString('en-us', {  weekday: 'long' });
}